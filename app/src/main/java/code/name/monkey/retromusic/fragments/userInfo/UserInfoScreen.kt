/*
 * Copyright (c) 2020 Hemanth Savarla.
 *
 * Licensed under the GNU General Public License v3
 *
 * This is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 */

package code.name.monkey.retromusic.fragments.userInfo

import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.result.contract.ActivityResultContracts
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.safeDrawingPadding
import androidx.compose.material.Button
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.OutlinedTextField
import androidx.compose.material.Scaffold
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.material.TopAppBar
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.rounded.ArrowBack
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import code.name.monkey.retromusic.R
import code.name.monkey.retromusic.funkwhale_lib.util.AuthorizationServiceFactory
import code.name.monkey.retromusic.funkwhale_lib.util.FuelResult
import code.name.monkey.retromusic.funkwhale_lib.util.OAuth
import code.name.monkey.retromusic.funkwhale_lib.util.Userinfo
import code.name.monkey.retromusic.funkwhale_lib.repository.FUserRepository
import com.google.accompanist.themeadapter.material.MdcTheme
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

data class FwCredentials(val token: String, val non_field_errors: List<String>?)

class UserInfoScreen : Fragment() {

    private val userInfoViewModel: UserInfoViewModel by viewModels()

    private val oAuth = OAuth.getInstance()
    private fun authedLogin(hostname: String): FuelResult {
        oAuth.init(hostname)
        return oAuth.register {
            FUserRepository.getInstance().setHostname(hostname);

            resultLauncher.launch(oAuth.authorizeIntent(requireActivity()))

        }
    }
    private var resultLauncher =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
            result.data?.let {
                oAuth.exchange(requireActivity(),it) {

                    lifecycleScope.launch(Dispatchers.Main) {
                        Userinfo.get(requireActivity(),oAuth)?.let {
                            //_navigateToMain.value = true
                        } ?: run {
                            //_showError.value = "login_error_userinfo" //getString(R.string.login_error_userinfo)
                        }
                    }
                }
            }
        }

    @Composable
    fun UserInfoLayout(modifier: Modifier = Modifier) {
        val userInfoUiState by userInfoViewModel.uiState.collectAsState()
        var domainInput by remember { mutableStateOf("") }
        var isLoggedIn = oAuth.isAuthorized(requireContext())
        MdcTheme (context= requireContext()) {
            Surface (modifier= modifier){
                Scaffold(
                    topBar = {
                        TopAppBar(
                            navigationIcon = {IconButton(onClick = { findNavController().navigateUp() }) {
                                Icon(Icons.Rounded.ArrowBack, "")
                            }},
                            title = {
                                Text(
                                    text = stringResource(R.string.profile)
                                )
                            }
                        )
                    },
                ) { innerPadding ->
                    Column(
                        modifier = Modifier
                            .padding(innerPadding)
                            .fillMaxSize(),
                        horizontalAlignment = Alignment.CenterHorizontally,
                    ) {
                        Text(isLoggedIn.toString())
                        Spacer(modifier = Modifier.height(16.dp))
                        OutlinedTextField(
                            value = domainInput,
                            onValueChange = {
                                domainInput = it
                            },
                            label = { Text(text = "serveur url") }

                        )
                        Spacer(modifier = Modifier.height(16.dp))
                        Button(onClick = {

                            var hostname = domainInput.trim().trim('/')

                            //try {
                            /* validateHostname(hostname, cleartext.isChecked)?.let {
                                 hostnameField.error = it
                                 return@setOnClickListener
                             }*/

                            val uri = Uri.parse(hostname)
                            if (uri.scheme == null) {
                                hostname = "https://$hostname"
                            }

                            //hostnameField.error = ""

                            val fuelResult = authedLogin(hostname)

                            //hostnameField.error = mapFuelResultToError(fuelResult)

                        }) {
                            Text("Login")
                        }
                    }
                }
            }

        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        return ComposeView(requireContext()).apply {
            setContent {
                UserInfoLayout(Modifier.safeDrawingPadding())
            }
        }
    }
}
