/*
 * Copyright (c) 2020 Hemanth Savarla.
 *
 * Licensed under the GNU General Public License v3
 *
 * This is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 */
package code.name.monkey.retromusic.fragments.artists

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.safeDrawingPadding
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.Scaffold
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.material.TopAppBar
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.rounded.ArrowBack
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import code.name.monkey.retromusic.App
import code.name.monkey.retromusic.R
import coil.compose.AsyncImage
import com.google.accompanist.themeadapter.material.MdcTheme
import org.koin.core.parameter.parametersOf


class ArtistDetailsFragment : Fragment() {
    private val arguments by navArgs<ArtistDetailsFragmentArgs>()

    private val artistDetailsViewModel: ArtistDetailsViewModel by viewModels {
        ArtistDetailsViewModelFactory(
            repository = (requireActivity().application as App).artistRepository,
            artistId =  parametersOf(arguments.extraArtistId, null).get<Long>().toInt()
        )
    }

    @Composable
    fun ArtistDetailsLayout(modifier: Modifier = Modifier) {
        val state by artistDetailsViewModel.uiState.collectAsState()
        return Scaffold(
            modifier = modifier,
            topBar = {
                TopAppBar(
                    navigationIcon = {
                        IconButton(onClick = { findNavController().navigateUp() }) {
                            Icon(Icons.Rounded.ArrowBack, "")
                        }
                    },
                    title = {
                    }
                )
            },
        ) { innerPadding ->
            Column(
                modifier.padding(innerPadding)
            ) {
                val lazyListState = rememberLazyListState()
                Column(
                    modifier
                        .padding(all = 10.dp)
                        .background(color = Color.Gray)) {
                    Row {
                    }
                }

                Column(
                    modifier = Modifier
                        .verticalScroll(rememberScrollState())
                        .fillMaxSize()){
                    val artistName = state.artist?.name ?: "Unkown Artist"

                    //Cover full size
                    Text( text= artistName, fontSize = 30.sp)
                    Row {
                        Text("1 album")
                        Text("·")
                        Text("1 morceau")
                        Text("·")
                        Text("1 min 17")
                    }
                    Row (Modifier.fillMaxSize()){
                        Text("> Tout lire")
                        Text("x Aléatoire")
                    }
                    Text( "Album(s)", fontSize = 20.sp)
                    SimpleGridView(
                        columns = 2,
                        itemCount = state.albums.size
                    ) { index ->
                        val album = state.albums[index]
                        AsyncImage(
                            placeholder = painterResource(R.drawable.ic_album),
                            model = album.coverUrl,
                            contentDescription = "Translated description of what the image contains"

                        )
                        Text(album.title)
                    }
                    Text( "Morceau(x)", fontSize = 20.sp)
                    //- NAME duration ···
                    //Biographie
                    //bio blabla
                    //Auditeurs Scrobble
                    //N        N

                }


            }
        }
    }
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        return ComposeView(requireContext()).apply {
            setContent {
                MdcTheme (context= requireContext()) {
                    Surface {
                        ArtistDetailsLayout(Modifier.safeDrawingPadding())
                    }
                }
            }
        }
    }
}

@Composable
fun SimpleGridView(
    modifier: Modifier = Modifier,
    columns: Int,
    itemCount: Int,
    content: @Composable() (index: Int) -> Unit
) {
    val columnAndRowItems = (0 until itemCount).chunked(columns)

    Column(modifier = modifier) {
        columnAndRowItems.forEach { rowItems ->
            Row(modifier = Modifier.fillMaxWidth()) {
                rowItems.forEach { index ->
                    Box(modifier = Modifier.weight(1f)) {
                        content(index)
                    }
                }
            }
        }
    }
}
