package code.name.monkey.retromusic.funkwhale_lib.util

import android.content.Context
import android.os.Build
import android.util.Log
import com.github.kittinunf.fuel.core.FuelError
import com.github.kittinunf.fuel.core.Request
import kotlinx.coroutines.CompletableDeferred
import kotlinx.coroutines.runBlocking
import net.openid.appauth.ClientSecretPost
import java.text.SimpleDateFormat
import java.util.Date

fun String?.containsIgnoringCase(candidate: String): Boolean =
  this != null && this.lowercase().contains(candidate.lowercase())

fun <T> Int.onApi(block: () -> T) {
  if (Build.VERSION.SDK_INT >= this) {
    block()
  }
}

fun <T, U> Int.onApi(block: () -> T, elseBlock: (() -> U)) {
  if (Build.VERSION.SDK_INT >= this) {
    block()
  } else {
    elseBlock()
  }
}

fun FuelError.formatResponseMessage(): String {
  return "${response.statusCode}: ${response.url}"
}
val ISO_8601_DATE_TIME_FORMAT = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")
fun Date.format(): String {
  return ISO_8601_DATE_TIME_FORMAT.format(this)
}

fun Request.authorize(context: Context, oAuth: OAuth): Request {
  return runBlocking {
    this@authorize.apply {
      if (!Settings.isAnonymous()) {
        oAuth.state().let { state ->
          state.accessTokenExpirationTime?.let {
            Log.i("Request.authorize()", "Accesstoken expiration: ${Date(it).format()}")
          }
          val old = state.accessToken
          val auth = ClientSecretPost(oAuth.state().clientSecret)
          val done = CompletableDeferred<Boolean>()
          val tokenService = oAuth.service(context)

          state.performActionWithFreshTokens(tokenService, auth) { token, _, e ->
            if (e != null) {
              Log.e("Request.authorize()", "performActionWithFreshToken failed: $e")
              Log.e("Request.authorize()", Log.getStackTraceString(e))
            }
            if (token == old) {
              Log.i("Request.authorize()", "Accesstoken not renewed")
            }
            if (token != old && token != null) {
              state.save()
            }
            header("Authorization", "Bearer ${oAuth.state().accessToken}")
            done.complete(true)
          }
          done.await()
          tokenService.dispose()
          return@runBlocking this
        }
      }
    }
  }
}
