package code.name.monkey.retromusic.funkwhale_lib.repository

import code.name.monkey.retromusic.funkwhale_lib.util.AppContext
import code.name.monkey.retromusic.funkwhale_lib.util.OAuth
import com.preference.PowerPreference

class FUserRepository private constructor() {

    private val oAuth = OAuth.getInstance()
    fun setHostname(hostname: String) {
        PowerPreference.getFileByName(AppContext.PREFS_CREDENTIALS).setString("hostname", hostname)
    }

    companion object {
        // The instance is kept as a private property in the companion object
        private var instance: FUserRepository? = null

        // Provide a global point of access to the instance
        fun getInstance(): FUserRepository {
            if (instance == null) {
                instance = FUserRepository()
            }
            return instance!!
        }
    }
}
