package code.name.monkey.retromusic.fragments.artists

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import code.name.monkey.retromusic.funkwhale_lib.repository.FArtistRepository
import code.name.monkey.retromusic.funkwhale_lib.util.AppContext
import code.name.monkey.retromusic.util.logE
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import org.koin.core.context.startKoin

class ArtistsViewModel(private val repository: FArtistRepository) : ViewModel() {

    private val _uiState = MutableStateFlow(ArtistsState())
    val uiState: StateFlow<ArtistsState> = _uiState.asStateFlow()


    private var artistCount : Int? = null
    fun startedScrolling() {
        val currentState = uiState.value
        if (currentState.loadingAll != LoadingState.Idle)
            return
        _uiState.update { it.copy(loadingAll = LoadingState.WaitingToStart) }
        if (currentState.loadingFirstPage == LoadingState.Loaded ) {
            artistCount?.let{
                fetchAll(artistCount = it)
            }
        }
    }

    private fun fetchAll(artistCount:Int) {
        var pageCount = artistCount/ AppContext.PAGE_SIZE
        if (artistCount%AppContext.PAGE_SIZE > 0) {
            pageCount++
        }
        if (pageCount < 2) return

        var errorCount = 0
        var successCount = 0
        var errorString = ""
        _uiState.update{ it.copy( loadingAll = LoadingState.Loading) }

        _uiState.update { it.copy(status = 0) }
        for (i in 2..pageCount) {
            fetchPage(page = i,
                onResponse = { _ , _ ->
                    successCount++
                    if (errorCount+successCount == pageCount-1) {
                        val loadState = if (errorCount == 0) LoadingState.Loaded else LoadingState.Error
                        _uiState.update {
                            it.copy(
                                loadingAll = if (errorCount > 0) LoadingState.Error else loadState,
                            )}
                    }
                },
                onError = { error ->
                    errorCount++
                    errorString = "($errorCount) $error"
                    if(errorCount+successCount == pageCount-1) {
                        _uiState.update { it.copy(error = errorString, loadingAll = LoadingState.Error) }
                    } else {
                        _uiState.update { it.copy(error = errorString) }

                    }
                }

            )
        }
    }
    private fun fetchPage(page: Int,
                          onResponse: (status: Int, count: Int) -> Unit,
                          onError: (String) -> Unit,) {
        viewModelScope.launch {
            _uiState.update { it.copy(artists = repository.getAllArtists()) }
            repository.fetchArtists(
                page = page,
                onResponse = onResponse,
                onError = onError
            )
        }
    }


    init {
        _uiState.update{ it.copy( loadingFirstPage = LoadingState.Loading) }
        logE("loginglog1")
        repository.getAllArtistsFlow()
            .onEach { value -> _uiState.update { it.copy(artists = value) } }
            .launchIn(viewModelScope)
        viewModelScope.launch {

            _uiState.update { it.copy(artists = repository.getAllArtists()) }

            repository.fetchArtists(
                page = 1,
                onResponse = {status, count -> _uiState.update {
                    it.copy(
                        status = status,
                        loadingFirstPage = if (it.loadingAll.hasError) LoadingState.Error else LoadingState.Loaded,
                    )}
                    artistCount = count
                },
                onError= {error -> _uiState.update { it.copy(error = error, loadingFirstPage = LoadingState.Error) }}
            )
        }
    }
}

class ArtistsViewModelFactory(private val repository: FArtistRepository) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(ArtistsViewModel::class.java)) {
            @Suppress("UNCHECKED_CAST")
            return ArtistsViewModel(repository) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}
