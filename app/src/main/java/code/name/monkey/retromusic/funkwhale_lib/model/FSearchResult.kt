package code.name.monkey.retromusic.funkwhale_lib.model

interface FSearchResult {
  fun cover(): String?
  fun title(): String
  fun subtitle(): String
}
