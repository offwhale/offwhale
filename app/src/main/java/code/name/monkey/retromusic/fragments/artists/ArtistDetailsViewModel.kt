/*
 * Copyright (c) 2020 Hemanth Savarla.
 *
 * Licensed under the GNU General Public License v3
 *
 * This is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 */
package code.name.monkey.retromusic.fragments.artists

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import code.name.monkey.retromusic.funkwhale_lib.repository.FArtistRepository
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch

class ArtistDetailsViewModel(
    repository: FArtistRepository,
    artistId: Int,
) : ViewModel() {

    private val _uiState = MutableStateFlow(ArtistDetailsState())
    val uiState: StateFlow<ArtistDetailsState> = _uiState.asStateFlow()
    init {
        viewModelScope.launch {
            val artist = repository.getArtistWithAlbums(artistId=artistId)
            _uiState.update { it.copy(artist = artist.artist, albums = artist.albums) }
        }
    }
}
class ArtistDetailsViewModelFactory(
    private val repository: FArtistRepository,
    private val artistId: Int,
) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(ArtistDetailsViewModel::class.java)) {
            @Suppress("UNCHECKED_CAST")
            return ArtistDetailsViewModel(repository, artistId) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}
