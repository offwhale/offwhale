package code.name.monkey.retromusic.funkwhale_lib.db

import androidx.room.Dao
import androidx.room.Query
import androidx.room.Transaction
import androidx.room.Upsert
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.StateFlow


@Dao
interface ArtistDao {
    @Upsert
    suspend fun upsertArtist(artist: ArtistEntity)

    @Query("SELECT * FROM ArtistEntity ORDER BY name")
    suspend fun getArtists(): List<ArtistEntity>
    @Query("SELECT * FROM ArtistEntity ORDER BY name")
    fun getArtistsFlow(): Flow<List<ArtistEntity>>
    @Transaction
    @Query("SELECT * FROM ArtistEntity")
    fun getArtistsWithAlbums(): List<ArtistWithAlbums>
    @Transaction
    @Query("SELECT * FROM ArtistEntity WHERE id = :artistId")
    suspend fun getArtistWithAlbums(artistId: Int): ArtistWithAlbums

    @Upsert
    fun upsertAll(artists: List<ArtistEntity>)
    @Upsert
    fun upsertAllAlbums(albums: List<AlbumEntity>)

}
