package code.name.monkey.retromusic.fragments.userInfo

data class UserInfoState (
    val username: String = "",
    val isLoggedIn: Boolean = false,
)