package code.name.monkey.retromusic.fragments.userInfo

import androidx.lifecycle.ViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow

class UserInfoViewModel : ViewModel() {
    private val _uiState = MutableStateFlow(UserInfoState())
    val uiState: StateFlow<UserInfoState> = _uiState.asStateFlow()
}
