package code.name.monkey.retromusic.funkwhale_lib.model

data class FRadio(
  val id: Int,
  var radio_type: String,
  val name: String,
  val description: String,
  var related_object_id: String? = null
)
