package code.name.monkey.retromusic.funkwhale_lib.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class FCovers(val urls: FCoverUrls) : Parcelable
