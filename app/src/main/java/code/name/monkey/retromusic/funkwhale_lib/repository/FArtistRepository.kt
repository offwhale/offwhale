package code.name.monkey.retromusic.funkwhale_lib.repository

import android.content.Context
import android.net.Uri
import androidx.annotation.WorkerThread
import code.name.monkey.retromusic.funkwhale_lib.db.AlbumEntity
import code.name.monkey.retromusic.funkwhale_lib.db.ArtistDao
import code.name.monkey.retromusic.funkwhale_lib.db.ArtistEntity
import code.name.monkey.retromusic.funkwhale_lib.db.ArtistWithAlbums
import code.name.monkey.retromusic.funkwhale_lib.model.FArtistsResponse
import code.name.monkey.retromusic.funkwhale_lib.util.AppContext
import code.name.monkey.retromusic.funkwhale_lib.util.OAuth
import code.name.monkey.retromusic.funkwhale_lib.util.authorize
import com.github.kittinunf.fuel.Fuel
import com.github.kittinunf.fuel.core.ResponseDeserializable
import com.github.kittinunf.fuel.core.extensions.authentication
import com.google.gson.Gson
import kotlinx.coroutines.flow.Flow


class FResponse
class Deserializer : ResponseDeserializable<FArtistsResponse> {
    override fun deserialize(content: String): FArtistsResponse
            = Gson().fromJson(content, FArtistsResponse::class.java)
}
class FArtistRepository(private val artistDao: ArtistDao, private val appContext: Context) {
    private val oAuth = OAuth.getInstance()

    suspend fun getAllArtists(): List<ArtistEntity> {
        return artistDao.getArtists()
    }
    fun getAllArtistsFlow(): Flow<List<ArtistEntity>> {
        return artistDao.getArtistsFlow()
    }
    suspend fun getArtistWithAlbums(artistId: Int): ArtistWithAlbums {
        return artistDao.getArtistWithAlbums(artistId = artistId)
    }

    @Suppress("RedundantSuspendModifier")
    @WorkerThread
    suspend fun fetchArtists(
        page: Int,
        onResponse: (status: Int, count: Int) -> Unit,
        onError: (String) -> Unit,
    )  {
        val url = Uri.parse("https://music.intbh.space/api/v1/artists/")
            .buildUpon()
            .appendQueryParameter("playable", "true")
            .appendQueryParameter("ordering", "name")
            .appendQueryParameter("has_albums", "true")
            .appendQueryParameter("page_size", AppContext.PAGE_SIZE.toString())
            .appendQueryParameter("page", page.toString())
            .appendQueryParameter("scope", "all")
            .build().toString()

        Fuel.get(url)
            .authorize(appContext, oAuth)
            .responseObject(Deserializer()) { _, response, result ->
                try {
                    val result = result.get()
                    val artists = result.results
                    val artistEntities = artists.map {
                        ArtistEntity(id = it.id, name = it.name)
                    }
                    val albums: List<AlbumEntity> = artists.flatMap { artist ->
                        artist.albums?.map { album ->
                            AlbumEntity(
                                id = album.id,
                                artistId = artist.id,
                                title = album.title,
                                coverUrl = album.cover?.urls?.original,
                                releaseDate = album.release_date,
                            )
                        } ?: emptyList()
                    }

                    artistDao.upsertAll(artistEntities)
                    artistDao.upsertAllAlbums(albums = albums)

                    onResponse(response.statusCode, result.count)
                } catch (e: Exception) {
                    onError(e.localizedMessage)
                }
            }
    }



}
