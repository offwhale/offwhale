package code.name.monkey.retromusic.funkwhale_lib.model
import android.os.Parcelable

import kotlinx.parcelize.Parcelize
@Parcelize
data class FAlbum(
  val id: Int,
  val artist: Int,
  val title: String,
  val cover: FCovers?,
  val release_date: String?,
) : Parcelable

@Parcelize
data class FAlbumFull(
  val id: Int,
  val artist: Artist,
  val title: String,
  val cover: FCovers?,
  val release_date: String?
) : FSearchResult, Parcelable {
  @Parcelize
  data class Artist(val name: String) : Parcelable

  override fun cover() = cover?.urls?.original
  override fun title() = title
  override fun subtitle() = artist.name
}