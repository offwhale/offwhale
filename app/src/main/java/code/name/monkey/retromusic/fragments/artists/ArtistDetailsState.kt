package code.name.monkey.retromusic.fragments.artists

import code.name.monkey.retromusic.funkwhale_lib.db.AlbumEntity
import code.name.monkey.retromusic.funkwhale_lib.db.ArtistEntity

data class ArtistDetailsState (
    val artist: ArtistEntity? = null,
    val albums: List<AlbumEntity> = ArrayList(),
)