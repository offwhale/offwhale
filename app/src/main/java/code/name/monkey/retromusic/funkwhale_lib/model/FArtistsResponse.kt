package code.name.monkey.retromusic.funkwhale_lib.model

data class FArtistsResponse(
  override val count: Int,
  override val next: String?,
  val results: List<FArtist>
) : FFAResponse<FArtist>() {
  override fun getData() = results
}
