package code.name.monkey.retromusic.funkwhale_lib.repository

import code.name.monkey.retromusic.funkwhale_lib.model.FArtist

sealed class CacheItem<D : Any>(val data: List<D>)

class ArtistsCache(data: List<FArtist>) : CacheItem<FArtist>(data)/*
class AlbumsCache(data: List<Album>) : CacheItem<Album>(data)
class TracksCache(data: List<Track>) : CacheItem<Track>(data)
class FavoritesCache(data: List<Favorite>) : CacheItem<Favorite>(data)
class PlaylistsCache(data: List<Playlist>) : CacheItem<Playlist>(data)
class PlaylistTracksCache(data: List<PlaylistTrack>) : CacheItem<PlaylistTrack>(data)
class RadiosCache(data: List<Radio>) : CacheItem<Radio>(data)
class FavoritedCache(data: List<Int>) : CacheItem<Int>(data)
class QueueCache(data: List<Track>) : CacheItem<Track>(data)*/
