package code.name.monkey.retromusic.fragments.artists

import code.name.monkey.retromusic.funkwhale_lib.db.ArtistEntity

data class ArtistsState (
    val artists: List<ArtistEntity> = ArrayList(),
    val status: Int? = null,
    val error: String? = null,
    val loadingFirstPage: LoadingState = LoadingState.Idle,
    val loadingAll: LoadingState = LoadingState.Idle,
)

enum class LoadingState {

    Idle, WaitingToStart, Loading, Loaded, Error;
    val  hasError get() = this == Error
}