package code.name.monkey.retromusic.funkwhale_lib.model

import com.google.android.exoplayer2.offline.Download

data class FDownloadInfo(
  val id: Int,
  val contentId: String,
  val title: String,
  val artist: String,
  var download: Download?
)
