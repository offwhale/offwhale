package code.name.monkey.retromusic.funkwhale_lib.model

import android.os.Parcelable
import code.name.monkey.retromusic.funkwhale_lib.db.ArtistEntity
import kotlinx.parcelize.Parcelize
import java.util.Calendar.DAY_OF_YEAR
import java.util.GregorianCalendar

@Parcelize
data class FArtist(
  val id: Int,
  val name: String,
  val albums: List<FAlbum>?
) : FSearchResult, Parcelable {

  override fun cover(): String? = albums?.mapNotNull { it.cover?.urls?.original }?.let { covers ->
    if (covers.isEmpty()) {
      return@let null
    }
    // Inject a little whimsy: rotate through the album covers daily
    val index = GregorianCalendar().get(DAY_OF_YEAR) % covers.size
    covers.getOrNull(index)
  }

  override fun title() = name
  override fun subtitle() = "Artist"


  fun  toDatabaseEntity(): ArtistEntity {
    return ArtistEntity(id= id, name = name)
  }
}
