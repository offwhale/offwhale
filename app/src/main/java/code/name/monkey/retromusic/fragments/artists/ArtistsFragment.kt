/*
 * Copyright (c) 2020 Hemanth Savarla.
 *
 * Licensed under the GNU General Public License v3
 *
 * This is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 */
package code.name.monkey.retromusic.fragments.artists

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.safeDrawingPadding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.Scaffold
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.material.TopAppBar
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.rounded.ArrowBack
import androidx.compose.material.ripple.rememberRipple
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.snapshotFlow
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.findNavController
import androidx.navigation.fragment.FragmentNavigatorExtras
import androidx.navigation.fragment.findNavController
import code.name.monkey.retromusic.App
import code.name.monkey.retromusic.EXTRA_ARTIST_ID
import code.name.monkey.retromusic.R
import code.name.monkey.retromusic.model.Artist
import code.name.monkey.retromusic.util.logE
import com.google.accompanist.themeadapter.material.MdcTheme

class ArtistsFragment : Fragment() {

    private val artistsViewModel: ArtistsViewModel by viewModels {
        ArtistsViewModelFactory((requireActivity().application as App).artistRepository)
    }

    @Composable
    fun ArtistsLayout(modifier: Modifier = Modifier) {
        val state by artistsViewModel.uiState.collectAsState()
        return Scaffold(
            modifier = modifier,
            topBar = {
                TopAppBar(
                    navigationIcon = {
                        IconButton(onClick = { findNavController().navigateUp() }) {
                            Icon(Icons.Rounded.ArrowBack, "")
                        }
                    },
                    title = {
                        Text(text = stringResource(R.string.profile))
                    }
                )
            },
        ) { innerPadding ->
            Column(
                modifier.padding(innerPadding)
            ) {
                val lazyListState = rememberLazyListState()
                Column(
                    modifier
                        .padding(all = 10.dp)
                        .background(color = Color.Gray)) {
                    Row {
                        Text("Status: ${state.status.toString()}", color = Color.Blue)
                        Spacer(modifier = Modifier.size(10.dp))
                        Text( state.loadingFirstPage.name)
                        Spacer(modifier = Modifier.size(10.dp))
                        Text( state.loadingAll.name)
                        Spacer(modifier = Modifier.size(10.dp))
                        Text( state.artists.size.toString())
                    }
                    state.error?.let {
                        Text(state.error.toString(), color = Color.Red.copy(alpha = 0.5f))
                    }
                }

                LazyColumn(
                    state = lazyListState,
                    modifier = Modifier.fillMaxSize()) {
                    items(state.artists.size) {
                        val artist = state.artists[it]
                        Box(
                            modifier = Modifier.padding(all = 10.dp)
                                .clickable(
                                    onClick = {
                                        findNavController().navigate(
                                            R.id.artistDetailsFragment,
                                            bundleOf(EXTRA_ARTIST_ID to artist.id.toLong())
                                        )
                                    }
                                )) {
                            Text(artist.name)
                        }
                    }
                }

                LaunchedEffect(state) {
                    snapshotFlow { lazyListState.isScrollInProgress }
                        .collect {
                            if (it) artistsViewModel.startedScrolling()
                        }
                }

            }
        }
    }
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        return ComposeView(requireContext()).apply {
            setContent {
                MdcTheme (context= requireContext()) {
                    Surface {
                        ArtistsLayout(Modifier.safeDrawingPadding())
                    }
                }
            }
        }
    }
}
